package com.yohan.oraculo.Realm.Rotinas;

import com.yohan.oraculo.Realm.Models.RealmPerguntas;
import com.yohan.oraculo.Realm.Models.RealmValidador;

import io.realm.Realm;
import io.realm.RealmQuery;

public class RealmConsultaPerg {

    private String resposta;
    private int valido;

    public String resposta(String pergunta) {
        //iniciando consulta no realm
        Realm realm = Realm.getDefaultInstance();


        RealmPerguntas realmPerguntas = new RealmPerguntas();
        realm.beginTransaction();

        RealmPerguntas result = realm.where(RealmPerguntas.class).equalTo("pergunta", pergunta).findFirst();

        try{
           resposta = result.getResposta();
            valido  = result.getValida();
        }catch(Exception e){
           resposta = "X" ;
        }

        if (valido == 0){
            resposta = "X" ;
        }



        realm.commitTransaction();

        realm.close();

        return resposta;
    }
}
