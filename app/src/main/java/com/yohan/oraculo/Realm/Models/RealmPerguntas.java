package com.yohan.oraculo.Realm.Models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RealmPerguntas extends RealmObject {
    @PrimaryKey
    private int id;
    private String pergunta;
    private String resposta;
    private int   valida;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPergunta() {
        return pergunta;
    }

    public void setPergunta(String pergunta) {
        this.pergunta = pergunta;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    public int getValida() {
        return valida;
    }

    public void setValida(int valida) {
        this.valida = valida;
    }
}
