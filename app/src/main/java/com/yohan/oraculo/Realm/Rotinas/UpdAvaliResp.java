package com.yohan.oraculo.Realm.Rotinas;

import com.yohan.oraculo.Realm.Models.RealmValidador;

import io.realm.Realm;

public class UpdAvaliResp {
    private int qtde;
    private String feedback;
    private String suc;

    public String Sucesso(String AvalResp) {
        //Salvando contador banco local

        Realm realm = Realm.getDefaultInstance();


        RealmValidador realmValidador = new RealmValidador();
        realm.beginTransaction();

        RealmValidador result = realm.where(RealmValidador.class).equalTo("id", 1).findFirst();
        try {
            qtde = result.getQtdePerg();
            feedback = result.getFeedback();
            AvalResp = result.getAvalResp();
            //Toast.makeText(getApplicationContext(), qtde + " Peguntas feitas", Toast.LENGTH_LONG).show();
        }catch (Exception e){
           // qtde = 1;
            feedback = "N";
            AvalResp = "X";
            //Toast.makeText(getApplicationContext(), qtde + "Peguntas feitas", Toast.LENGTH_LONG).show();
            //Toast.makeText(getApplicationContext(), qtde + "Peguntas feitas", Toast.LENGTH_LONG).show();
        }

        if (qtde == 0) {
            realmValidador.setId(1);
            realmValidador.setQtdePerg(0);
            realmValidador.setFeedback("N");
            realmValidador.setAvalResp("X");
            realm.copyToRealm(realmValidador);


        } else{
                realmValidador.setAvalResp(AvalResp);
                realm.copyToRealmOrUpdate(realmValidador);

        }
        realm.commitTransaction();

        suc = "ok";
        return suc;
    }


}

