package com.yohan.oraculo.Realm.Config;

import com.yohan.oraculo.Realm.Models.RealmPerguntas;
import com.yohan.oraculo.Realm.Models.RealmValidador;

import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.annotations.PrimaryKey;

public class MyMigration implements RealmMigration {
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion){
        RealmSchema realmSchema = realm.getSchema();

        if (oldVersion == 0){
            realmSchema.get("RealmValidador")
                    .addField("avalResp", String.class);
            oldVersion++;
        }

        if (oldVersion == 1){
            RealmObjectSchema perguntasSchema = realmSchema.create("RealmPerguntas")
                    .addField("id", int.class, FieldAttribute.PRIMARY_KEY)
                    .addField("pergunta", String.class)
                    .addField("resposta", String.class);

            oldVersion++;
        }


        if(oldVersion == 2){
            RealmObjectSchema estatisticasSchema = realmSchema.create("RealmEstatisticas")
                    .addField("id", int.class, FieldAttribute.PRIMARY_KEY)
                    .addField("amor", int.class)
                    .addField("emprego", int.class)
                    .addField("dinheiro", int.class);
            oldVersion++;
        }

        if(oldVersion == 3){
            realmSchema.get("RealmPerguntas")
                    .addField("valido", int.class);
            oldVersion++;
        }
    }
}
