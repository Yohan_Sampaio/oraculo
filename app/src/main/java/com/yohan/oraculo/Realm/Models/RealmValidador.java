package com.yohan.oraculo.Realm.Models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RealmValidador extends RealmObject {
    @PrimaryKey
    private int id;
    private int qtdePerg;
    private String feedback;
    private String avalResp;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQtdePerg() {
        return qtdePerg;
    }

    public void setQtdePerg(int qtdePerg) {
        this.qtdePerg = qtdePerg;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getAvalResp() {return avalResp;    }

    public void setAvalResp(String avalResp) {this.avalResp = avalResp;    }
}
