package com.yohan.oraculo.Realm.Models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RealmEstatisticas extends RealmObject {
    @PrimaryKey
    private int id;
    private int amor;
    private int emprego;
    private int dinheiro;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmor() {
        return amor;
    }

    public void setAmor(int amor) {
        this.amor = amor;
    }

    public int getEmprego() {
        return emprego;
    }

    public void setEmprego(int emprego) {
        this.emprego = emprego;
    }

    public int getDinheiro() {
        return dinheiro;
    }

    public void setDinheiro(int dinheiro) {
        this.dinheiro = dinheiro;
    }
}
