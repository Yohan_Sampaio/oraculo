package com.yohan.oraculo;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ApresentacaoActivity extends AppCompatActivity {
    private TextView txtEaster;
    private Button btnGo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apresentacao);

        btnGo = (Button) findViewById(R.id.btnApresenGo);
        txtEaster = (TextView) findViewById(R.id.txtApresentEaster);

        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ApresentacaoActivity.this, PrincipalActivity.class);
                startActivity(intent);
            }
        });

        txtEaster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateDialog();
            }
        });

    }

    public void CreateDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(ApresentacaoActivity.this)
                .setTitle("Aviso!")
                .setMessage("Se você deisistir, não irá encontrar as repostas!")
                .setPositiveButton("Continuar",null)
                .setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        btnGo.setFocusable(true);
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
