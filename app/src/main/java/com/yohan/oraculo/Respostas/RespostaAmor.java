package com.yohan.oraculo.Respostas;

import com.yohan.oraculo.Realm.Models.RealmEstatisticas;

import java.util.Random;

import io.realm.Realm;

/**
 * Created by Suporte01 on 06/12/2017.
 */

public class RespostaAmor {
    private String respostaCerta, amor;
    private int idPerg;

    public String respostaAmor(int id) {
        String[] resposta = {"O amor é um sentimento único, não confunda paixão com amor.",
                "Ame-se em primeiro lugar.",
                "Talvez você devesse perguntar para essa pessoa.",
                "Sim."};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        amor = "X";
        AlteraEstat();
        return respostaCerta;


    }

    public String respostaNamoro(int id) {
        String[] resposta = {"Acredite em mim, você deve perguntar para a pessoa?",
                "Sim",
                "Um relacionamento deve partir de um dos lados, tome a iniciativa."};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        amor = "X";
        AlteraEstat();
        return respostaCerta;
    }

    public String respostaCasamento(int id) {
        String[] resposta = {"Tudo pode acontecer quando existe amor",
                "Sim", "Não",
                "Tenha paciência, que tudo tem sem tempo."};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        amor = "X";
        AlteraEstat();
        return respostaCerta;
    }

    public String respostaGravida(int id) {
        String[] resposta = {"Se você fez algo e não se protegeu, existe uma grande chance disso. Mas isso é bom.",
                "Sim", "Não"};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];

        return respostaCerta;
    }

    public String respostaTerFilho(int id) {
        String[] resposta = {"Se você deseja isso, sugiro começar a praticar.",
                "Sim", "Não"};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        return respostaCerta;

    }
    public String respostaEsperandoFilho(int id) {
        String[] resposta = {"Está tudo bem, pode acreditar!", "Não há nada de errado"};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        return respostaCerta;

    }

    public String respostaPensando(int id) {
        String[] resposta = {"Pode acreditar que sim!", "Sim!", "Só pensa em você"};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        amor = "X";
        AlteraEstat();
        return respostaCerta;

    }

    public String respostaTraicao(int id) {
        String[] resposta = {"Não pense nisso.", "O diálogo entre vocês pode resolver qualquer problema."};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        amor = "X";
        AlteraEstat();
        return respostaCerta;

    }

    private void AlteraEstat(){
        Realm realm = Realm.getDefaultInstance();
        RealmEstatisticas realmEstatisticas = new RealmEstatisticas();
        realm.beginTransaction();

        RealmEstatisticas result = realm.where(RealmEstatisticas.class).equalTo("id",1).findFirst();
        try {
            if (amor.equals("X")){
                int amorInt = result.getAmor();
                amorInt++;
                realmEstatisticas.setAmor(amorInt);
            }
        }catch (Exception e ){
            realmEstatisticas.setId(1);
            if (amor.equals("X")) {
                realmEstatisticas.setAmor(1);
            }
            realmEstatisticas.setDinheiro(0);
            realmEstatisticas.setEmprego(0);
        }

        realm.copyToRealmOrUpdate(realmEstatisticas);
        realm.commitTransaction();
        realm.close();
    }
}
