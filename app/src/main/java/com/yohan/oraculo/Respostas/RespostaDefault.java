package com.yohan.oraculo.Respostas;

import java.util.Random;

/**
 * Created by Suporte01 on 12/12/2017.
 */

public class RespostaDefault {
    private String respostaCerta;
    private int idPerg;

    public String respostaDefault (int id){
        String[] resposta = {"Negativo",
                "Sinto em dizer, mas a resposta é não.",
                "Sim.", "Não", "Claro"};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        return respostaCerta;

    }

    public String respostaCurta (int id){
        String[] resposta = {"Desculpe, não entendi",
                "Tente ser mais específico."};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        return respostaCerta;

    }

}
