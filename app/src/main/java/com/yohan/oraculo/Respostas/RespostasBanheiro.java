package com.yohan.oraculo.Respostas;

import java.util.Random;

public class RespostasBanheiro {
    private String respostaCerta;
    private int idPerg;

    public String respostaPeido (int id){
        String[] resposta = {"Não passe vontade.",
                "Não se preocupe com os outros, agora é a hora",
                "Vá ao banheiro imediatamente."};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);

        respostaCerta = resposta[idPerg];

        return respostaCerta;

    }

    public String respostaCagar (int id){
        String[] resposta = {"Você deve estar suando frio, é bom correr.",
                "Ninguém bate na porta duas vezes, #FicaDica",
                "Vá ao banheiro imediatamente."};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);

        respostaCerta = resposta[idPerg];

        return respostaCerta;

    }
}

