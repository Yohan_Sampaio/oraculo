package com.yohan.oraculo.Respostas;

import java.util.Random;

public class RespostasOraculo {
    private String respostaCerta;
    private int idPerg;

    public String respostaFilme (int id){
        String[] resposta = {"Meu filme preferido é Star Wars, em especifico a Ameaça Fantasma."};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        return respostaCerta;

    }

    public String respostaCor (int id){
        String[] resposta = {"Gosto de Verde"};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        return respostaCerta;

    }

    public String respostaComida (int id){
        String[] resposta = {"Uhm, qualquer coisa, desde que não tenha cebola."};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        return respostaCerta;

    }

    public String respostaMusica (int id){
        String[] resposta = {"Gosto da nona sinfonia de Bethoven."};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        return respostaCerta;

    }

    public String respostaOi(int id){
        String[] resposta = {"Olá...",
                "Opa..."};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        return respostaCerta;

    }
    public String respostaTudoBem(int id){
        String[] resposta = {"Tudo sim, alguém se importa comigo.",
                "Tranquilo", "Mais ou Menos, estou com uns problemas.", "Não, preciso desabafar."};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        return respostaCerta;

    }
    public String respostaMentira(int id){
        String[] resposta = {"Você ousa dúvidar de minha sabedoria?", "Ás vezes, só quando me incomodam, igual você está fazendo agora."};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        return respostaCerta;

    }
    public String respostaVerdade(int id){
        String[] resposta = {"Você ousa dúvidar de minha sabedoria?", "Ás vezes, só quando me incomodam, igual você está fazendo agora."};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        return respostaCerta;

    }
    public String respostaCorRoupa(int id){
        String[] resposta = {"Desculpe, mas sou daltônico então não sei afirmar com exatidão, mas acho que é verde",
                "Desculpe, mas sou daltônico então não sei afirmar com exatidão, mas acho que é preto",
                "Desculpe, mas sou daltônico então não sei afirmar com exatidão, mas acho que é rosa",
                "Desculpe, mas sou daltônico então não sei afirmar com exatidão, mas acho que é amarelo",
                "Desculpe, mas sou daltônico então não sei afirmar com exatidão, mas acho que é cinza"};
        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        return respostaCerta;

    }
}
