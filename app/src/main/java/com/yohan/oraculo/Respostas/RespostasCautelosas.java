package com.yohan.oraculo.Respostas;

import java.util.Random;

/**
 * Created by Suporte01 on 12/12/2017.
 */

public class RespostasCautelosas {
    private String respostaCerta;
    private int idPerg;

    public String respostaMorte (int id){


        respostaCerta = "Essa não é a melhora saída. Se Precisar de ajuda fale com alguém ou ligue para 188";

        return respostaCerta;

    }

    public String respostaBater (int id){

        String[] resposta = {"Você não deve bater em ninguém."};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        return respostaCerta;



    }

    public String respostaMorrer (int id){

        String[] resposta = {"A única certeza que existe é sobre a Morte, mas não há necessidade de adiantar isso."};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        return respostaCerta;



    }

    public String respostaBeber (int id){

        String[] resposta = {"Você é forte e conseguirá."};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        return respostaCerta;



    }

    public String respostaVicio (int id){

        String[] resposta = {"Você é forte e conseguirá.",
                "Acredite, você irá conseguir parar"};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        return respostaCerta;



    }


}
