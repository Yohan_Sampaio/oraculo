package com.yohan.oraculo.Respostas;

import com.yohan.oraculo.Realm.Models.RealmEstatisticas;

import io.realm.Realm;

public class RespostasSouEu {
    private int amor, dinheiro, emprego;
    private String respoosta;

    public String QuemSouEu(int id){

        Realm realm = Realm.getDefaultInstance();
        RealmEstatisticas realmEstatisticas = new RealmEstatisticas();

        realm.beginTransaction();

        RealmEstatisticas result = realm.where(RealmEstatisticas.class).equalTo("id", 1).findFirst();

        try{
            amor        = result.getAmor();
            dinheiro    = result.getDinheiro();
            emprego     = result.getEmprego();
        }catch (Exception e){
            respoosta = "Ainda é muito cedo para dizer quem é você, continue perguntando, depois me pergunte novamete.";
        }

        if((amor > dinheiro) && (amor > emprego)){
            if (dinheiro > emprego){
                respoosta = "Você é uma pessoa que tem muito amor e um pouco ambiciosa, continue assim.";
            }else{
                if (emprego > dinheiro) {
                    respoosta = "Você é uma pessoa que tem muito amor, mas que se preocupa com sua atual situação profissional";
                }else{
                    respoosta = "Você é uma pessoa que aparenta ser muito amorosa";
                }
            }

        }
        if ((dinheiro > amor) && (dinheiro > emprego)){
            if (amor > emprego){
                respoosta = "Você é uma pessoa que sonha em ser bem sucedido financeiramente e amorosa, continue assim.";
            }else {
                if (amor < emprego) {
                    respoosta = "Você é uma pessoa que sonha em ser bem sucedido financeiramente e para isso busca um crescimento profissional";
                }else{
                    respoosta = "Você é uma pessoa que sonha em ser bem sucedido financeiramente, uma dica, nunca deixe de acreditar.";
                }
            }
        }

        if ((emprego > amor) && (emprego > dinheiro)){
            if (emprego > amor){
                respoosta = "Você é uma pessoa que está procurando crescer profissionalmente e bastante amarosa, acredite e conseguirá";
            }else{
                if (emprego < amor) {
                    respoosta = "Você é uma pessoa que está procurando crescer profissionalmente e com isso vem o sucesso financeiro, acredite e conseguirá";
                }else{
                    respoosta = "Você é uma pessoa que está procurando crescer profissionalmente e isso é muito bom, pode acreditar você vai conseguir.";
                }
            }
        }

        if ((emprego == 0) && (amor == 0) && (dinheiro == 0)){
            respoosta = "Ainda é muito cedo para dizer quem é você, continue perguntando, depois me pergunte novamete.";
        }

        realm.close();

        return respoosta;
    }
}
