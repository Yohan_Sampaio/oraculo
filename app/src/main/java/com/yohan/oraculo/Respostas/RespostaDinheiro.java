package com.yohan.oraculo.Respostas;

import com.yohan.oraculo.Realm.Models.RealmEstatisticas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import io.realm.Realm;

/**
 * Created by Suporte01 on 12/12/2017.
 */

public class RespostaDinheiro {
    private String respostaCerta, dinh, empr;
    private int idPerg;
    private int n1,n2,n3,n4,n5,n6;


    public String respostaRico (int id){
        String[] resposta = {"Se esforce que você conseguirá.",
                "Nunca deixe de acreditar.",
                "Você está fazendo algo para que isso aconteça?",
                "Sim.", "Não",
                "Acredito que você tenha uma ideia de como chegar lá, então por que não tenta?"};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        dinh = "X";
        empr = "";
        AlteraEstat();
        return respostaCerta;

    }
    public String respostaEmpregoAtual (int id){
        String[] resposta = {"As vezes devemos sonhar mais alto, reflita sobre isso!",
        "Se você não está feliz, devesse tentar algo novo."};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        dinh = "X";
        empr = "";
        AlteraEstat();
        return respostaCerta;

    }
    public String respostaNovoEmprego (int id){
        String[] resposta = {"Sim",
                "Não"};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        empr = "X";
        dinh = "";
        AlteraEstat();
        return respostaCerta;

    }
    public String respostaPobre (int id){
        String[] resposta = {"A vida pode tomar vários sentidos, mas lembre-se que você está no controle.",
                "Não"};

        Random mRandom = new Random();
        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = resposta[idPerg];
        dinh = "X";
        empr = "";
        AlteraEstat();
        return respostaCerta;

    }

    public String respostaNumLoterias (int id){
        String[] resposta = {"Sim"};
        Random mRandom = new Random();

        List<Integer> numeros = new ArrayList<Integer>();
        for (int i = 1; i < 61; i++) { //Sequencia da mega sena
            numeros.add(i);
        }

        Collections.shuffle(numeros); //embaralha os números
        for(int i=1 ;i < 7; i++){
            if (n1 == 0){
                n1 = numeros.get(i);
            }
            if ((n2 == 0) && (i == 2)){
                n2 = numeros.get(i);
            }
            if ((n3 == 0) && (i == 3)){
                n3 = numeros.get(i);
            }
            if ((n4 == 0) && (i == 4)){
                n4 = numeros.get(i);
            }
            if ((n5 == 0) && (i == 5)){
                n5 = numeros.get(i);
            }
            if ((n6 == 0) && (i == 6)){
                n6 = numeros.get(i);
            }
        }


        idPerg = mRandom.nextInt(resposta.length);
        respostaCerta = "Seus números da sorte são: " + n1 + "-" + n2 + "-" + n3 + "-" + n4 + "-" + n5 + "-" + n6;
        return respostaCerta;

    }

    private void AlteraEstat(){
        Realm realm = Realm.getDefaultInstance();
        RealmEstatisticas realmEstatisticas = new RealmEstatisticas();

        realm.beginTransaction();

        RealmEstatisticas result = realm.where(RealmEstatisticas.class).equalTo("id",1).findFirst();


        try {
            if (dinh.equals("X")){
                int dinhInt = result.getDinheiro();
                dinhInt++;
                realmEstatisticas.setDinheiro(dinhInt);
            }
            if (empr.equals("X")) {
                int emprInt = result.getEmprego();
                emprInt++;
                realmEstatisticas.setEmprego(emprInt);
            }

        }catch (Exception e ){
            realmEstatisticas.setId(1);
            realmEstatisticas.setAmor(0);
            if (dinh.equals("X")) {
                realmEstatisticas.setDinheiro(0);
            }
            if (empr.equals("X")) {
                realmEstatisticas.setEmprego(0);
            }
        }

        realm.copyToRealmOrUpdate(realmEstatisticas);
        realm.commitTransaction();
        realm.close();
    }
}
