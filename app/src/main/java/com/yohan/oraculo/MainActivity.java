package com.yohan.oraculo;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.yohan.oraculo.Realm.Models.RealmEstatisticas;
import com.yohan.oraculo.Realm.Models.RealmPerguntas;

import io.realm.Realm;

public class MainActivity extends AppCompatActivity {
    private Button btnEntrar;
    private TextView txtPoliti;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        txtPoliti = (TextView) findViewById(R.id.txtPoliti);

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ApresentacaoActivity.class);
                startActivity(intent);
            }
        });

        txtPoliti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Termos.class);
                startActivity(intent);
            }
        });

    }


}
