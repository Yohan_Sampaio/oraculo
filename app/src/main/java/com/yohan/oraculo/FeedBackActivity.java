package com.yohan.oraculo;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.yohan.oraculo.Identidade.IdNoveDigitos;
import com.yohan.oraculo.Realm.Models.RealmValidador;

import io.realm.Realm;

public class FeedBackActivity extends AppCompatActivity {
    private EditText edtFeedback;
    private Button btnEnviar;
    private int id, qtde;
    private String comentario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);

        edtFeedback = (EditText) findViewById(R.id.edtFeedback);
        btnEnviar = (Button) findViewById(R.id.btnEnviarFeed);

        final IdNoveDigitos geraId = new IdNoveDigitos();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference mRef = database.getReference("FeedBack");

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edtFeedback.getText().length() > 5){
                    comentario = edtFeedback.getText().toString();
                    id = geraId.idPk(1);
                    mRef.child(Integer.toString(id)).setValue(comentario);

                    Realm realm = Realm.getDefaultInstance();
                    RealmValidador realmValidador = new RealmValidador();

                    RealmValidador result = realm.where(RealmValidador.class).equalTo("id", 1).findFirst();
                    id = result.getId();
                    qtde = result.getQtdePerg();

                    realm.beginTransaction();

                    realmValidador.setId(1);
                    realmValidador.setQtdePerg(qtde);
                    realmValidador.setFeedback("S");
                    realm.copyToRealmOrUpdate(realmValidador);

                    realm.commitTransaction();
                    realm.close();

                    CreateDialog();
                }else{
                   CreateDialogErro();
                }



            }
        });

    }

    public void CreateDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(FeedBackActivity.this)
                .setTitle("FeedBack!")
                .setMessage("Agradecemos o Feedback!")
                .setPositiveButton("Continuar",null)
                .setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(FeedBackActivity.this, PrincipalActivity.class);
                        startActivity(intent);
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
    public void CreateDialogErro(){
        AlertDialog.Builder builder = new AlertDialog.Builder(FeedBackActivity.this)
                .setTitle("Aviso!")
                .setMessage("Seu comentário deve ser um pouco maior")
                .setPositiveButton("Continuar",null)
                .setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        edtFeedback.setFocusable(true);
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
