package com.yohan.oraculo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.media.Rating;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.hsalf.smilerating.BaseRating;
import com.hsalf.smilerating.SmileRating;
import com.yohan.oraculo.Análises.PerguntaAnalise;
import com.yohan.oraculo.Identidade.IdNoveDigitos;
import com.yohan.oraculo.Realm.Models.RealmPerguntas;
import com.yohan.oraculo.Realm.Models.RealmValidador;
import com.yohan.oraculo.Realm.Rotinas.RealmConsultaPerg;
import com.yohan.oraculo.Realm.Rotinas.UpdAvaliResp;
import com.yohan.oraculo.Respostas.RespostaDefault;

import org.w3c.dom.Text;

import java.util.Calendar;

import io.realm.Realm;
import io.realm.RealmResults;

public class PrincipalActivity extends AppCompatActivity {
    private Button btnPerguntar;
    private EditText edtPergunta;
    private TextView txtResposta;
    private String pergunta, perg1, dia, mes, ano;
    private String resposta, chaveDia, feedback, rating, nota;
    private AdView mAdView;
    private int  id, qtde, i, pergId;
    private String AvalResp;
    private String emotion;
    private String loteria;
    private Object o ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);


        MobileAds.initialize(this, "ca-app-pub-3561119433496626~6137562046");


        btnPerguntar = (Button) findViewById(R.id.btnPerguntar);
        edtPergunta = (EditText) findViewById(R.id.edtPergunta);
        txtResposta = (TextView) findViewById(R.id.txtResposta);
        mAdView = (AdView) findViewById(R.id.adView);

        ActionBar actionBar = getSupportActionBar();
        //getActionBar().setDisplayHomeAsUpEnabled(true);


        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        //Obtendo o dia de hoje
        Calendar cal = Calendar.getInstance();
        dia = Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
        mes = Integer.toString(cal.get(Calendar.MONTH));
        ano = Integer.toString(cal.get(Calendar.YEAR));
        chaveDia = dia + ' ' + mes + ' ' + ano;


        edtPergunta.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_GO) {
                    perguntar();
                    return true;
                }
                return false;
            }
        });

        edtPergunta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdView.setVisibility(View.INVISIBLE);
            }
        });


        btnPerguntar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                perguntar();

            }
        });



    }



    public void CreateDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(PrincipalActivity.this)
                .setTitle("Aviso!")
                .setMessage("Eu sei de tudo, mas para que exista reposta é necessário uma pergunta!")
                .setPositiveButton("Continuar",null)
                .setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        edtPergunta.setFocusable(true);
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void CreateDialogHelp(){
        AlertDialog.Builder builder = new AlertDialog.Builder(PrincipalActivity.this)
                .setTitle("Ajuda!")
                .setMessage("Exemplos de Pergunta: 'vou ser Rico?', 'Sou Amada?'")
                .setPositiveButton("Continuar",null)
                .setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        edtPergunta.setFocusable(true);

                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static void hideKeyboard(Context context, View editText) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public void perguntar(){

        final RespostaDefault respostaDefault = new RespostaDefault();

        final PerguntaAnalise mAnalise = new PerguntaAnalise();

        final IdNoveDigitos geraId = new IdNoveDigitos();

        final RealmConsultaPerg consulPer = new RealmConsultaPerg();


        //Inserido no Banco de Dados
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mRef = database.getReference(chaveDia);

        mAdView.setVisibility(View.VISIBLE);

        if(edtPergunta.length() > 1) {
            perg1 = edtPergunta.getText().toString();
            if (perg1.equals(pergunta)){
                //Mantem mesma resposta
            }else {
                pergunta = edtPergunta.getText().toString();
                resposta = consulPer.resposta(pergunta);
                if (resposta == "X") {
                    resposta = mAnalise.resposta(pergunta);
                    //resposta = o.equals();
                }


                if (resposta.equals("0")) {// retorno da análise for vazia ele mostra uma resposta padrão
                    if (resposta.length() < 8) {
                        resposta = respostaDefault.respostaCurta(1);
                    }else{
                        resposta = respostaDefault.respostaDefault(1);
                    }
                }

                txtResposta.setText(resposta);

                id = geraId.idPk(1);

                mRef.child(Integer.toString(id)).child("pergunta").setValue(pergunta);
                mRef.child(Integer.toString(id)).child("resposta").setValue(resposta);

                //Salvando contador banco local

                Realm realm = Realm.getDefaultInstance();


                RealmValidador realmValidador = new RealmValidador();
                realm.beginTransaction();

                RealmValidador result = realm.where(RealmValidador.class).equalTo("id", 1).findFirst();
                try {
                    qtde = result.getQtdePerg();
                    feedback = result.getFeedback();
                    AvalResp = result.getAvalResp();
                    if (AvalResp.equals(null)){
                        AvalResp = "X";
                    }

                   // Toast.makeText(getApplicationContext(), AvalResp + " Avalia Resposta", Toast.LENGTH_LONG).show();
                   // Toast.makeText(getApplicationContext(), qtde + "Peguntas ", Toast.LENGTH_LONG).show();
                }catch (Exception e){
                     qtde = 0;
                     feedback = "N";
                     AvalResp = "X";
                    //Toast.makeText(getApplicationContext(), "" + e, Toast.LENGTH_LONG).show();
                }

                if (qtde == 0) {
                    realmValidador.setId(1);
                    realmValidador.setQtdePerg(1);
                    realmValidador.setFeedback("N");
                    realmValidador.setAvalResp("X");
                    realm.copyToRealmOrUpdate(realmValidador);

                    //Toast.makeText(getApplicationContext(), qtde + "Peguntas ", Toast.LENGTH_LONG).show();
                } else{
                    if (feedback.equals("N")) {
                        qtde = qtde + 1;
                        realmValidador.setId(1);
                        realmValidador.setQtdePerg(qtde);
                        realmValidador.setFeedback("N");
                        realmValidador.setAvalResp(AvalResp);
                        realm.copyToRealmOrUpdate(realmValidador);
                       // Toast.makeText(getApplicationContext(), qtde + "Peguntas feitas", Toast.LENGTH_LONG).show();
                    }
                }

                realm.commitTransaction();
                realm.close();

                SalvaPergunta(loteria);




            }
            hideKeyboard(getBaseContext(), edtPergunta);
            qtdePerguntas();

            if (AvalResp.equals("X")&& (qtde != 10 && qtde != 25 && qtde != 45)){
                RespostaFeedback();
            }

            if (AvalResp.equals("S") && (qtde != 10 && qtde != 25 && qtde != 45)){
                Handler handler = new Handler();

                Runnable run = new Runnable() {
                    @Override
                    public void run() {
                        DialogNota();
                    }
                };

                handler.postDelayed(run, 3000);
            }

        }else{
            CreateDialog();
            hideKeyboard(getBaseContext(), edtPergunta);
            qtdePerguntas();
        }
    }

    public void qtdePerguntas(){
        Realm realm = Realm.getDefaultInstance();
        RealmValidador realmValidador = new RealmValidador();
        realmValidador = realm.where(RealmValidador.class).equalTo("id", 1).findFirst();
        qtde = realmValidador.getQtdePerg();
        String feed = realmValidador.getFeedback();
        if(!feed.equals("S")){
            switch (qtde){
                case  10:
                    DialogFeedback();
                    break;

                case 25:
                    DialogFeedback();
                    break;

                case 40:
                    DialogFeedback();
                    break;
            }
        }

    }

    public void DialogFeedback(){
        AlertDialog.Builder builder = new AlertDialog.Builder(PrincipalActivity.this)
                .setTitle("FeedBack!")
                .setMessage("Está gostando do aplicativo? Mostre para gente seu FeedBack")
                .setPositiveButton("Gostei",null)
                .setPositiveButton("Gostei", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=oraculo.projeto.com.oraculo"); // missing 'http://' will cause crashed
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                }).setNegativeButton("Não Gostei", null)
                .setNegativeButton("Não Gostei", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent =  new Intent(PrincipalActivity.this, FeedBackActivity.class);
                        startActivity(intent);
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void RespostaFeedback(){

        AlertDialog.Builder builder = new AlertDialog.Builder(PrincipalActivity.this)
                .setTitle("Avalie a Resposta!")
                .setMessage("Gostaria de ajudar o Oráculo a crescer? É simples basta avaliar as repostas dadas.")
                .setPositiveButton("Claro",null)
                .setPositiveButton("Claro", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Realm realm = Realm.getDefaultInstance();
                        RealmValidador realmValidador = new RealmValidador();


                        RealmValidador result = realm.where(RealmValidador.class).equalTo("id", 1).findFirst();
                        realm.beginTransaction();

                        realmValidador.setId(1);
                        realmValidador.setQtdePerg(qtde);
                        realmValidador.setFeedback("N");
                        realmValidador.setAvalResp("S");
                        realm.copyToRealmOrUpdate(realmValidador);

                        realm.commitTransaction();

                    }
                }).setNegativeButton("O Oráculo que se vire", null)
                .setNegativeButton("O Oráculo que se vire", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Realm realm = Realm.getDefaultInstance();
                        RealmValidador realmValidador = new RealmValidador();

                        RealmValidador result = realm.where(RealmValidador.class).equalTo("id", 1).findFirst();

                        realm.beginTransaction();

                        realmValidador.setId(1);
                        realmValidador.setQtdePerg(qtde);
                        realmValidador.setFeedback("N");
                        realmValidador.setAvalResp("N");
                        realm.copyToRealmOrUpdate(realmValidador);

                        realm.commitTransaction();


                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();

       // Toast.makeText(this, "AvalResp" + AvalResp, Toast.LENGTH_SHORT).show();

    }

    public void DialogNota(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final SmileRating rating = new SmileRating(this);
        rating.setNameForSmile(BaseRating.BAD,"Ruim");
        rating.setNameForSmile(BaseRating.GOOD,"Bom");
        rating.setNameForSmile(BaseRating.GREAT,"Ótimo");
        rating.setNameForSmile(BaseRating.OKAY,"Razoável");
        rating.setNameForSmile(BaseRating.TERRIBLE,"Péssimo");
//        rating.setNameForSmile(BaseRating.NONE,"Indiferente");


        //builder.setIcon(android.R.drawable.btn_star_big_on);
        builder.setTitle("Avalie a Resposta ");
        builder.setView(rating);

        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Inserido no Banco de Dados
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference mRef = database.getReference(chaveDia);

                        int smile = rating.getSelectedSmile();
                        switch (smile){
                            case SmileRating.BAD:
                                emotion = "Ruim";
                                break;
                            case SmileRating.GOOD:
                                emotion = "Bom";
                                break;
                            case SmileRating.GREAT:
                                emotion = "Ótimo";
                                break;
                            case SmileRating.OKAY:
                                emotion = "Razoavel";
                                break;
                            case SmileRating.TERRIBLE:
                                emotion = "Péssimo";
                                break;
                            case SmileRating.NONE:
                                emotion = "Nada";
                                break;
                        }

                        mRef.child(Integer.toString(id)).child("Nota").setValue(emotion);

                    }
                })
                .setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

        builder.create();
        builder.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_actions, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_configuration:
                Intent intent = new Intent(PrincipalActivity.this, ConfigActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_help:
                CreateDialogHelp();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void SalvaPergunta(String valido){
        //Salvando perguntas no banco local

        //Toast.makeText(getBaseContext(),"Valido " + valido,Toast.LENGTH_LONG).show();

        Realm realm = Realm.getDefaultInstance();

        RealmPerguntas realmPerguntas = new RealmPerguntas();
        realm.beginTransaction();

        try{
            RealmPerguntas result = realm.where(RealmPerguntas.class).findAll().last();
            pergId = result.getId();
        }catch (Exception e){
            pergId = 0;
        }

        pergId++;
        realmPerguntas.setId(pergId);
        realmPerguntas.setPergunta(pergunta);
        realmPerguntas.setResposta(resposta);
        if (valido != "X"){
            realmPerguntas.setValida(1);
        }else{
            realmPerguntas.setValida(0);
        }




        realm.copyToRealmOrUpdate(realmPerguntas);

        realm.commitTransaction();
        realm.close();


    }
}

