package com.yohan.oraculo;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.yohan.oraculo.Realm.Models.RealmValidador;

import io.realm.Realm;

public class ConfigActivity extends AppCompatActivity {
    private Switch switchAvalResp;
    private Button btnSalvar, btnReportar;
    private TextView txtAvalResp;
    private String avalResp, feedback;
    private int  id, qtde, i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        btnSalvar = findViewById(R.id.btnSalvarConfig);
        btnReportar = findViewById(R.id.btnReportBug);
        switchAvalResp = findViewById(R.id.switchAvalResp);
        txtAvalResp = findViewById(R.id.txtSwitchAvalResp);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        qtde = 0;
        feedback = "";
        avalResp = "";

        switchAvalResp.setText("Avalia Resposta");

        avalResp =  AvalResp();

        if (avalResp.equals("S")){
            txtAvalResp.setText("Sim");
            switchAvalResp.setChecked(true);
        }else{
            txtAvalResp.setText("Não");
            switchAvalResp.setChecked(false);
        }


        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean check = switchAvalResp.isChecked();
                if (check){
                    avalResp = "S";
                }else{
                    avalResp = "N";
                }
                try {

                    Realm realm = Realm.getDefaultInstance();
                    RealmValidador realmValidador = new RealmValidador();
                    realm.beginTransaction();

                    RealmValidador result = realm.where(RealmValidador.class).equalTo("id", 1).findFirst();

                    qtde = result.getQtdePerg();
                    feedback = result.getFeedback();

                    realmValidador.setId(1);
                    realmValidador.setQtdePerg(qtde);
                    realmValidador.setFeedback(feedback);
                    realmValidador.setAvalResp(avalResp);
                    realm.copyToRealmOrUpdate(realmValidador);
                    realm.commitTransaction();

                    realm.close();

                    Toast.makeText(ConfigActivity.this, "Alteração Salva", Toast.LENGTH_SHORT).show();

                }catch (Exception e){
                    Toast.makeText(ConfigActivity.this, "Ooops, algo deu errado ao tentar salvar." + e, Toast.LENGTH_LONG).show();
                }
            }
        });

        switchAvalResp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(switchAvalResp.isChecked()){
                    txtAvalResp.setText("Sim");
                }else{
                    txtAvalResp.setText("Não");
                }
            }
        });

        btnReportar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfigActivity.this, FeedBackActivity.class);
                startActivity(intent);
            }
        });

    }


    public String AvalResp() {

        final Realm realm = Realm.getDefaultInstance();


        final RealmValidador realmValidador = new RealmValidador();
        realm.beginTransaction();

        RealmValidador result = realm.where(RealmValidador.class).equalTo("id", 1).findFirst();
        try {
            avalResp = result.getAvalResp();
        }catch (Exception e){
            avalResp = "N";
        }

        realm.commitTransaction();

        realm.close();

        return avalResp;
    }
}


