package com.yohan.oraculo.Análises;

import com.yohan.oraculo.Respostas.RespostaAmor;
import com.yohan.oraculo.Respostas.RespostaDefault;
import com.yohan.oraculo.Respostas.RespostaDinheiro;
import com.yohan.oraculo.Respostas.RespostasBanheiro;
import com.yohan.oraculo.Respostas.RespostasCautelosas;
import com.yohan.oraculo.Respostas.RespostasObjeto;
import com.yohan.oraculo.Respostas.RespostasOraculo;
import com.yohan.oraculo.Respostas.RespostasSexualidade;
import com.yohan.oraculo.Respostas.RespostasSouEu;

/**
 * Created by Suporte01 on 06/12/2017.
 */

public class PerguntaAnalise {
   private String respostaPerg;
   private String loteriaPerg;




    public String resposta (String pergunta){

       RespostaAmor respostaAmor                    = new RespostaAmor();
       RespostaDinheiro respostaDineheiro           = new RespostaDinheiro();
       RespostasCautelosas respostasCautelosas      = new RespostasCautelosas();
       RespostaDefault respostaDefault              = new RespostaDefault();
       RespostasBanheiro respostasBanheiro          = new RespostasBanheiro();
       RespostasSexualidade respostasSexualidade    = new RespostasSexualidade();
       RespostasOraculo respostasOraculo            = new RespostasOraculo();
       RespostasObjeto respostasObjeto              = new RespostasObjeto();
       RespostasSouEu respostasSouEu                = new RespostasSouEu();




       respostaPerg = "0";
       loteriaPerg = "";
       //--------------------------------------------Perguntas Amor------------------------------------------------
       //Valida se pergunta é sobre amor
      if((pergunta.contains("amor")) || (pergunta.contains("gosta")) || (pergunta.contains("ama")) ||
      (pergunta.contains("afim"))){

          respostaPerg =  respostaAmor.respostaAmor(1);

      }
      //Valida se pergunta é sobre namoro
      if((pergunta.contains("namorar")) || (pergunta.contains("ficar"))){

          respostaPerg = respostaAmor.respostaNamoro(1);

      }
        //Valida se pergunta é sobre casamento
      if((pergunta.contains("casar") || (pergunta.contains("casamento")))){

          respostaPerg = respostaAmor.respostaCasamento(1);

      }
       //Valida se pergunta é sobre pensado
       if((((pergunta.contains("pensando")) || (pergunta.contains("pensar")))
               && ((pergunta.contains("nos")) || (pergunta.contains("nós")) || (pergunta.contains("mim"))))){

           respostaPerg = respostaAmor.respostaPensando(1);

       }
       //Valida se pergunta é sobre Gravida
       if(((pergunta.contains("gravida") ||(pergunta.contains("grávida"))) && (pergunta.contains("estou")))){

           respostaPerg = respostaAmor.respostaGravida(1);

       }
       //Valida se pergunta é sobre ter filhos
       if(((pergunta.contains("filhos")) ||(pergunta.contains("filho"))) && (pergunta.contains("ter"))){

           respostaPerg = respostaAmor.respostaTerFilho(1);

       }
       //Valida se pergunta é sobre os filhos
       if(((pergunta.contains("filhos") ||(pergunta.contains("filho"))) && ((pergunta.contains("meus")) || (pergunta.contains("meu")))
        && ((pergunta.contains("bem")) || (pergunta.contains("mal")) || (pergunta.contains("estão")) || (pergunta.contains("está"))
               || (pergunta.contains("estao")) || (pergunta.contains("esta"))))){

           respostaPerg = respostaAmor.respostaEsperandoFilho(1);

       }
       //Valida se pergunta é sobre traição
       if(((pergunta.contains("pessoa"))  || (pergunta.contains("mulher"))|| (pergunta.contains("homem")))
               && (pergunta.contains("ter")) && ((pergunta.contains("outra")) ||(pergunta.contains("outro")))){

           respostaPerg = respostaAmor.respostaTraicao(1);

       }
       //Valida se pergunta é sobre traição
       if((pergunta.contains("traicao")) ||(pergunta.contains("traiçao")) || (pergunta.contains("triacao")) || (pergunta.contains("trair"))){

           respostaPerg = respostaAmor.respostaTraicao(1);

       }

       //--------------------------------------------Perguntas Dinheiro-----------------------------------------------

      //Valida se pergunta é sobre ser Rico
       if((((pergunta.contains("rico")) || (pergunta.contains("rica"))) || (pergunta.contains("dinheiro"))) && ((pergunta.contains("muito")) || (pergunta.contains("ser")) || (pergunta.contains("ter")))
               && (!(pergunta.contains("perder"))) ){

           respostaPerg = respostaDineheiro.respostaRico(1);

       }
       //Valida se pergunta é sobre ser Rico
       if((((pergunta.contains("procurar")) || (pergunta.contains("ter")))&& (pergunta.contains("emprego")))){

           respostaPerg = respostaDineheiro.respostaEmpregoAtual(1);

       }
       //Valida se pergunta é sobre NOVO EMPREGO
       if((((pergunta.contains("novo")) || (pergunta.contains("arrumar")) || (pergunta.contains("conseguir")))
               && (pergunta.contains("emprego")))){

           respostaPerg = respostaDineheiro.respostaNovoEmprego(1);

       }
       //Valida se pergunta é sobre ser pobre
       if((pergunta.contains("pobre")) && (pergunta.contains("ser"))){

           respostaPerg = respostaDineheiro.respostaPobre(1);

       }
       //Valida se pergunta é sobre ser Rico
       if(((pergunta.contains("numeros")) || (pergunta.contains("números")) || (pergunta.contains("numero")))
               && ((pergunta.contains("loteria")) || (pergunta.contains("sena")))){

           respostaPerg = respostaDineheiro.respostaNumLoterias(1);
           loteriaPerg = "X";

       }

       //---------------------------------------------Perguntas cautelosas---------------------------------------

       //Valida se pergunta é de Suicidio
       if ((pergunta.contains("matar"))  || ((pergunta.contains("vida")) && (pergunta.contains("tira"))
       || (pergunta.contains("suicidar")))){

          respostaPerg = respostasCautelosas.respostaMorte(1);
       }
       //Valida se pergunta é de Bater
       if ((pergunta.contains("agredir")) || (pergunta.contains("bater")) || ((pergunta.contains("socar")) && (pergunta.contains("esfaquear"))
               || (pergunta.contains("chutar")))){

           respostaPerg = respostasCautelosas.respostaBater(1);
       }
       //Valida se pergunta é de Morrer
       if (pergunta.contains("morrer")){

           respostaPerg = respostasCautelosas.respostaMorrer(1);
       }
       //Valida se pergunta é sobre para de beber
       if(((pergunta.contains("beber")) && (pergunta.contains("parar")))){

           respostaPerg = respostasCautelosas.respostaBeber(1);

       }
       //Valida se pergunta é sobre parar com vicio
       if((((pergunta.contains("drogas")) || (pergunta.contains("fumar")) ) && (pergunta.contains("parar")))){

           respostaPerg = respostasCautelosas.respostaVicio(1);

       }

       //-----------------------------------------Perguntas Oraculo ---------------------------------------------

       //Valida se pergunta é sobre Musica Oraculo
       if((pergunta.contains("musica")) ){

           respostaPerg = respostasOraculo.respostaMusica(1);

       }
       //Valida se pergunta é sobre Oráculo e Comida
       if((pergunta.contains("comida")) || (pergunta.contains("prato")) ){

           respostaPerg = respostasOraculo.respostaComida(1);

       }
       //Valida se pergunta é sobre Oráculo e Filme
       if((pergunta.contains("filme")) ){

           respostaPerg = respostasOraculo.respostaFilme(1);

       }
       //Valida se pergunta é sobre Oraculo e cor
       if((pergunta.contains("cor")) && (pergunta.contains("sua"))){

           respostaPerg = respostasOraculo.respostaCor(1);

       }
       //Valida se pergunta é sobre Oraculo ser Mentira
       if((((pergunta.contains("você")) || (pergunta.contains("voce")) || (pergunta.contains("vc"))) && ((pergunta.contains("mentira")) || (pergunta.contains("mentiroso"))))){

           respostaPerg = respostasOraculo.respostaMentira(1);

       }
       //Valida se pergunta é sobre Oraculo ser verdade
       if((((pergunta.contains("você")) || (pergunta.contains("voce")) || (pergunta.contains("vc"))) && ((pergunta.contains("verdade")) || (pergunta.contains("verdadeiro"))))){

           respostaPerg = respostasOraculo.respostaVerdade(1);

       }
       //Valida se pergunta é sobre oraculo e oi
       if((pergunta.equals("oi") || (pergunta.equals("ola")) || (pergunta.equals("olá")) || (pergunta.equals("eai")))){

           respostaPerg = respostasOraculo.respostaOi(1);

       }
       //Valida se pergunta é sobre oraculo e tudo bem
       if(((pergunta.contains("tudo")) && (pergunta.contains("bem"))) || (pergunta.equals("como vai")) || (pergunta.equals("blz"))){

           respostaPerg = respostasOraculo.respostaTudoBem(1);

       }
       //Valida se pergunta é sobre cor de roupa
       if(((pergunta.contains("cor")) && (pergunta.contains("roupa")))){

           respostaPerg = respostasOraculo.respostaCorRoupa(1);

       }

       //----------------------------------------Respostas Objeto--------------------------------------------

       //Valida se pergunta é sobre Cachorro aqui
       if(((pergunta.contains("cachorro")) && (pergunta.contains("aqui")))){

           respostaPerg = respostasObjeto.respostaCachorro(1);

       }

       //---------------------------------------Repostas Sexualidade------------------------------------------
       //Valida se pergunta é sobre ser Gay
       if((pergunta.contains("gay")) || ((pergunta.contains("homosexual")) || (pergunta.contains("lesbica")))){

           respostaPerg = respostasSexualidade.respostaGay(1);

       }
       //Valida se pergunta é sobre ser Viado
       if((pergunta.contains("viado")) || ((pergunta.contains("bixa")) || (pergunta.contains("sapatão"))
               || (pergunta.contains("bicha")))){

           respostaPerg = respostasSexualidade.respostaCorrigir(1);

       }

       //---------------------------------------Repostas Banheiro------------------------------------------
       //Valida se pergunta é sobre Peido
       if((pergunta.contains("peidar")) || ((pergunta.contains("flatular")) || (pergunta.contains("peido")))){

           respostaPerg = respostasBanheiro.respostaPeido(1);

       }
       //Valida se pergunta é sobre Cagar
       if((pergunta.contains("cagar")) || ((pergunta.contains("coco")) || (pergunta.contains("diarreia"))
               || (pergunta.contains("suando frio")))){

           respostaPerg = respostasBanheiro.respostaCagar(1);

       }

       //------------------------------------Respostas Sou Eu-----------------------------------------------
       if ((pergunta.contains("quem")) && (pergunta.contains("sou")) && (pergunta.contains("eu"))){
          respostaPerg = respostasSouEu.QuemSouEu(1);
       }

       Object o = new String[]{respostaPerg, loteriaPerg};
       return respostaPerg;

   }
}
